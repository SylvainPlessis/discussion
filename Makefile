all: clean discute dialogue

clean:
	rm -f *.aux *.log *.out *.pdf

discute:
	pdflatex discute
	pdflatex discute
	rm -f *.aux *.out *.log

dialogue:
	pdflatex dialogue
	pdflatex dialogue
	rm -f *.aux *.out *.log
